import random
_DEBUG_ = False


def echo(message):
    if _DEBUG_:
        print(message)


class Animal:
    def __init__(self, a_type, a_gender, str_status, coord):
        self._type = a_type
        self._gender = a_gender
        self._coordinate = coord
        self._status = str_status
        if a_type == 'Bear':
            min = 10
            max = 20
        else:
            min = 0
            max = 9
        self._strength = random.randint(min, max)

    def set_status(self, str_status):
        self._status = str_status

    def get_status(self):
        return self._status

    def set_position(self, new_coords):
        self._coordinate = new_coords

    def fight(self, other):
        if self.get_strength() > other.get_strength():
            other.set_status('dead')
            print(f'{other} dies in the fight')
        else:
            self.set_status('dead')
            print(f'{self} dies in the fight')

    def mate(self, other):
        parent1 = self
        parent2 = other
        newborn = Animal(self.get_type(),
                         random.choice(['male', 'female']), 'newborn', -1)
        print(f'a newborn {newborn} is born: \
              parents are {parent1} and {parent2}')
        return (newborn, parent1, parent2)

    def get_type(self):
        return self._type

    def get_gender(self):
        return self._gender

    def get_strength(self):
        return self._strength

    def __str__(self):
        tp = self.get_type()
        gen = self.get_gender()
        strg = self.get_strength()
        crd = self._coordinate
        return (f'{id(self)} {tp} {gen} {strg} @ {crd} ')


# River class
class River:
    def __init__(self, length):
        assert(length > 0 and type(length) == int)
        self._contents = length * [None]
        self._length = length
        self._birthplace = []
        self._graveyard = []

    # getters
    # returns length of the river

    def get_length(self):
        self._length = len(self._contents)
        return len(self._contents)

    # returns contents of the river
    def get_contents(self):
        return (self._contents)

    # behaviors
    def get_populated(self, debug, mocks=None):
        for ix in range(self.get_length()):
            self._contents[ix] = []
            filling = None
            if debug:
                if mocks[ix] is not None:
                    val = mocks[ix]
                    filling = Animal(val[0], val[1], 'alive', ix)
            else:
                chance = random.choice([1, 0])
                if chance == 1:
                    filling = Animal(random.choice(['Bear', 'Fish']),
                                     random.choice(['male', 'female']),
                                     'alive', ix)
            self._contents[ix].append(filling)
            print(f'filling {filling} populated and placed at {ix}')
        pass

    def fluctuate(self, debug, mocks=None):
        for ix in range(self.get_length()):
            change = random.choice([-1, 0, 1])
            if ix == 0 and change == 1:
                change = 0
            if ix == self.get_length() - 1 and change == 1:
                change = 0
            if self._contents[ix] is [None]:
                change = 0
            print(f'{self._contents[ix]} gets moved by {change}')
            if debug:
                change = mocks[ix]
            if change != 0 and len(self._contents[ix]) != 0:
                element = self._contents[ix][0]
                print(element)
                self._contents[ix + change].append(element)
                element.set_position(ix+change)
                self._contents[ix][0] = None

    def resolve_anomalies(self):
        for oix, o_slot in enumerate(self.get_contents()):
            print('iteration', o_slot)
            while len(o_slot) > 1:
                print('anomaly', o_slot, len(o_slot))
                if o_slot[0] is None:
                    o_slot.pop(0)
                if o_slot[1] is None:
                    o_slot.pop(1)
                if (
                   o_slot[0].get_type() == o_slot[1].get_type() and
                   o_slot[0].get_gender() != o_slot[1].get_gender()
                   ):
                    # mate scenario
                    outcome = o_slot[0].mate(o_slot[1])
                    child = outcome[0]
                    self._birthplace.append([child])
                    child_placed = False
                    parent_placed = False
                    parent_to_place = outcome[2]
                    for ix, slot in enumerate(self.get_contents()):
                        if slot == [None]:
                            if not child_placed:
                                self._contents[ix] = self._birthplace[0]
                                loc = ix
                                child_placed = True
                            else:
                                if not (parent_placed):
                                    self._contents[ix] = [parent_to_place]
                                    parent_placed = True
                                    self._contents[oix] = [outcome[1]]
                    if child_placed is False:
                        self._contents.append(child)
                        loc = self.get_length()
                        child_placed = True
                    if parent_placed is False:
                        self._contents.append(o_slot[1])
                        parent_placed = True
                        print('parent placed at the end')
                    if child_placed:
                        print(f'newborn placed at {loc}')
                        self._birthplace.pop(0)
                    if parent_placed and child_placed:
                        o_slot = self._contents[oix]
                        break

                else:
                    # fight scenario
                    print(o_slot[0].fight(o_slot[1]))
                    fighters = [o_slot[0], o_slot[1]]
                    self.bring_out_dead(fighters, oix)

    def bring_out_dead(self, fighters, ix):
        for animal in fighters:
            if animal.get_status() == 'dead':
                animal.set_position = 'g'
                self._graveyard.append(animal)
                self._contents[ix].remove(animal)

    def place_cluster(self, list_of_animals):
        for animal in list_of_animals:
            placed = False
            for ix, slot in enumerate(self.get_contents()):
                print(slot)
                if len(slot) == 0:
                    self._contents[ix] = animal
                    animal.set_position = ix
                    placed = True
                    break
            if placed is False:
                self._contents.append(animal)
                animal.set_position = len(self._contents)

    def print_contents(self):
        echo('\nstart river')
        for index, slot in enumerate(self.get_contents()):
            if slot == []:
                echo(f'empty space @{index}')
            else:
                for contents in slot:
                    echo(f'{slot} : {contents} in slot {index}')
        echo('end river\n')


def main():
    if _DEBUG_:
        river_length = 6
    else:
        river_length = random.randint(2, 10)
    assert type(river_length) == int and river_length > 0
    named_river = River(river_length)
    if _DEBUG_:
        print("populating")
        named_river.get_populated(True, mocks=[['Bear', 'female'],
                                               ['Bear', 'male'], None, [
                                  'Fish', 'male'], ['Fish', 'male'],
            ['Fish', 'male']])
    else:
        named_river.get_populated(False)
    named_river.print_contents()

    if _DEBUG_:
        print("fluctuate")
        named_river.fluctuate(True, mocks=[0, -1, 0, 1, 0, -1])
    else:
        named_river.fluctuate(False)
    print('after fluctuation')
    print(named_river.print_contents())
    print('\nnow resolving anomalies')
    named_river.resolve_anomalies()
    print('\nshowing the outcome')
    print(named_river.get_contents())
    print("\nIn the graveyeard")
    print(named_river._graveyard)
    print("\nIn the birthyard")
    print(named_river._birthplace)


if __name__ == "__main__":

    main()
