import unittest
import json
import river as r

# function to read jsons for mocks


def read_json(file):
    with open(file, 'r') as file:
        data = file.read()
        # parse file
        obj = json.loads(data)
    return obj

# testing if it's positive and integers are accepted


class test_get_populated(unittest.TestCase):
    def test_valid(self):
        json = read_json('unit_json/get_populated.json')
        valid_lens = json['payload']
        successful_attempts = len(valid_lens)
        for value in valid_lens:
            river_length = value
            try:
                named_river = r.River(river_length)
                named_river.get_populated(False)
            except Exception:
                successful_attempts = successful_attempts - 1
            self.assertTrue(successful_attempts == len(valid_lens))


if __name__ == '__main__':
    unittest.main()
